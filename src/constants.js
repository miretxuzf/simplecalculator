export const tooltips = {
    "numberOfWaves": "A wave refers to a collection of the clusters that start the intervention at a given time point.",
    "timeZeros" : "Whether clusters in the first wave (i.e., the first element of clusters) receive intervention at time 0 or not.",
    "treatmentEffect": "Treatment effect is a difference in mean between control and treatment groups.",
    "extraTime": "Number of additional time steps beyond the standard SW CRT design.",
    "sigma": "Standard deviation when assuming Gaussian distribution",
    "observation": " Number of observations for each cluster at each time point (same for all clusters at all time points).",
    "ICC": "Measures tendency for observations from the same cluster to be similar.",
    "pooled": "A weighted average of each cluster's standard deviation.",
    "targetPower": "This is a target power. Check for the calculated power in the results section.",
    "timePoints": "Excluding the baseline"
};

export const terminologies = [
    {
        title: "Stepped wedge cluster randomised trial",
        content: `The stepped wedge cluster randomised trial is a new research study design that is increasingly being used in the evaluation of interventions. 
        The design involves random and sequential crossover of clusters from control to intervention until all clusters are exposed.
        \n It is a complex study design which can involve the need for robust evaluations with ethical or logistical constraints. 
        It is particularly suited to evaluations that do not rely on individual patient recruitment. As in all cluster trials, stepped wedge trials with individual recruitment 
        and without concealment of allocation lead to a risk of selection biases.
        \n In a stepped wedge design more clusters recieve the intervention towards the end of the study than in its early stages.
        This implies that the effect of the intervention might be confounded with any underlying temporal trend. 
        Sample size calculations and analysis must make allowance for both the clustered nature of the design and the confounding effect of time.`,
        reference: ["numberOfWaves", "timeZeros","timePoints"]
    },
    {
        title: "Clinical trial",
        content: `Clinical trial is a planned experiment in medical research that involves human participants and is designed to evaluate the effectiveness of the treatment on future patients. One of the characteristics of clinical trials is that the results obtained in clinical trials which are based on the limited sample of patients are used to predict the behaviour of new treatments in the general population of patients. The outcome of clinical trials explains the effectiveness of the treatment, its potential side effects and how the new treatment should be conducted to the future patients. `,
        reference: []
    },
    {
        title: "Sample size calculations",
        content: `There are two methods commonly used by statisticians for calculating the best sample size for clinical trials. The first method is to choose a sample size that makes the confidence interval narrow. A narrower confidence interval implies that there is a smaller room for variation in the observation in that interval, therefore the precision is higher. In other words, increasing the sample size will decrease the marge of error therefore reducing the width of the confidence interval and increase the precision. The precision of the treatment effect is associated with the confidence intervals in this type of sample size determination.\n
        The second method is based on the power analysis in which a minimum sample size required to detect the given magnitude of treatment effect with a given degree of confidence is calculated. There are four parameters in power analysis:\n
•	Sample size
•	Effect size
•	Significance level 
•	Power 
`,
        reference: ["targetPower"]
    }
]