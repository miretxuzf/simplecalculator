import Vue from 'vue'
import Router from 'vue-router'
import PowerCalculation from '../components/PowerCalculation'
import Terminology from '../components/Terminology'
import SampleSizeCalculation from "../components/SampleSizeCalculation"
import About from "../components/About";

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: !localStorage.getItem("lastPage") ? "/power" : localStorage.getItem("lastPage")
    },
    {
      path: '/power',
      component: PowerCalculation
    },
    {
      path: '/sample-size',
      component: SampleSizeCalculation
    },
    {
        path: '/terminology/:id?',
        component: Terminology,
    },
    {
        path: '/about',
        component: About
    }
  ]
})
router.beforeEach((to, from, next) => {
  if (!localStorage.getItem("lastPage")) {
    localStorage.setItem("lastPage", "/power")
  }
  if(from.path === to.path) {
    next();
    return
  }
  if (from.path === "/terminology" && (to.path === "/power" || to.path === "/sample-size")) {
      const lastPage = localStorage.getItem("lastPage");
      if (lastPage !== to.path) {
          localStorage.setItem("lastPage", to.path) 
          window.location.href = "/"
          return;
      }
  }
  if ((from.path === "/about" && to.path === "/power") 
  || (from.path === "/about" && to.path === "/sample-size") 
  || (to.path === "/sample-size" && from.path === "/power")
  || (from.path === "/sample-size" && to.path === "/power")) {
    localStorage.setItem("lastPage", to.path) 
    window.location.href = "/"
    return;
  }
  next();
});
export default router